const task3 = function(arr) {
  let output = {};
  for (let index = 1; index < arr.length; index++) {
    output[index] = Math.max(
      ...arr.map(
        (item, currentIndex) =>
          arr[(currentIndex + index) % arr.length] - arr[currentIndex]
      )
    );
  }
  return output;
};
console.log(task3([1, 3, 2, 7]));
