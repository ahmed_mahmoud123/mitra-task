/* For each number in the array I converted it into string and after that split it into array 
and reduce the array of digits to get the sum and check if the sum > 10 I converted the number into string 
and split it into array and concat 
*/
const task2 = function(arr) {
  let new_arr = [];
  arr.every(item => {
    let sum = item
      .toString()
      .split("")
      .reduce((a, b) => +a + +b);
    return sum > 10 && (new_arr = new_arr.concat(sum.toString().split("")));
  });
  console.log(new_arr);
  // to get the most frequent number of the new array, I used sort and filter
  return new_arr
    .sort(
      (a, b) =>
        new_arr.filter(v => v === a).length -
        new_arr.filter(v => v === b).length
    )
    .pop();
};

console.log(task2([5569, 7659, 345, 97, 88, 55998, 30, 665]));
