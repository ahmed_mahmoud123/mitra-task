const task1 = function(children = [], father = []) {
  return children.map((age, i) => {
    let currentAge = age;
    while (++father[i] != ++age * 2);
    return age - currentAge;
  });
};

console.log(task1([12, 14], [44, 44]));
